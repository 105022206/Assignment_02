
     var loadState = {
        preload: function () {
        // Load the progress bar image.
        // Loat game sprites.
        game.load.image('bullet', 'assets/bullet.png');
        game.load.image('enemyBullet', 'assets/enemy-bullet.png');
        //game.load.spritesheet('invader', 'assets/invader32x32x4.png', 32, 32);
        game.load.spritesheet('bosss', 'assets/boss_.png', 1024, 1024);
        game.load.spritesheet('invader', 'assets/monster1.png', 272, 136);        
        game.load.spritesheet('ship', 'assets/plan5.png', 185, 144);

        //game.load.image('ship', 'assets/my_2.png');
        game.load.spritesheet('kaboom', 'assets/blow.png', 54, 54);
        //game.load.spritesheet('kaboom', 'assets/explode.png', 128, 128);
        game.load.image('starfield', 'assets/starfield.jpg');
        game.load.image('littleship', 'assets/player.png');
        game.load.image('background', 'assets/background2.png');
        game.load.image('energybar', 'assets/solider.png');
        game.load.image('HP', 'assets/blood.png');
        game.load.audio('shoot', 'assets/shoot1.mp3');
        game.load.audio('background', 'assets/background.mp3');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('tracker', 'assets/epb_1.png');
        game.load.spritesheet('skill', 'assets/explosions_0.png', 96,78);
        game.load.image('mute', 'assets/mute.png');
        game.load.image('menu_', 'assets/load.png');
        game.load.image('newbullet', 'assets/myb_2.png');




        
         
        },
        create: function() {
        // Set some game settings.
        game.state.start('menu');
        }
        }; 