# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|N|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : [隨著遊戲時間增加敵人發射子彈的速度和數量會來越多，在殺了
                        足夠多敵人之後會有BOSS出現，並不會再出現怪物，當能量條滿
                        了之後，可以按下A來啟動技能。]
2. Animations : [怪物、BOSS都有自己的動畫，玩家在射擊時也會切換到射擊的動畫。]
3. Particle Systems : [玩家子彈打中怪物時會噴出粒子，打中BOSS時的粒子和打中怪物
                       不一樣。]
4. Sound effects : [有背景音樂和射擊時的音樂，按下遊戲畫面左邊的靜音圖案可以把兩
                    個音樂都關掉，在按一次就會打開]
5. Leaderboard : [NO]

# Bonus Functions Description : 
1. [BoSS] : [Boss有獨特的移動方式還有攻擊模式]
2. [子彈自動追蹤] : [在吃了藍色小道具之後就可以讓發射的子彈自動追擊]
3. [小幫手] : [在吃了紅色小飛機之後他就會在玩家身旁不停地射擊]
4. [powerful bullet]:[在吃了紫色的子彈之後子彈威力會變強，一下就能打死怪獸]

