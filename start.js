var player;
var aliens;
var bullets;
var bullets1;
var bulletTime = 0;
var help_bulletTime=0;
var cursors;
var fireButton;
var explosions;
var starfield;
//var score = 0;
var scoreString = '';
var scoreText;
var lives;
var enemyBullet;
var firingTimer = 0;
var stateText;
var livingEnemies = [];
var firedelay=2000;
var barProgress=20;
var helper;
var shoot;
var background
var emitter;
var skill_image;
var tracker;
var enemies;
var boss;
var boss_time=0;
var boss_firetime=20;
var pause;
var mute;
var newbullet;
var new_bullet=false;
var pause_bool=false;
var boss_exist=false;
var enable_help=false;
var track=false;


var mainState = {
    preload: function() {
    },
    create: function() {
        starfield = game.add.tileSprite(0, 0, 800, 600, 'starfield');
        game.physics.setBoundsToWorld();

        // enrey bar
        this.barProgress = 5;
        
        this.bar = this.add.bitmapData(128, 8);

        mybar=game.add.sprite(game.world.centerX - (this.bar.width * 0.5), 30, this.bar);
        
        //game.add.tween(this).to({barProgress: 0}, 2000, null, true, 0, Infinity);

        
        //  Our bullet group
    bullets = game.add.group();
    bullets.enableBody = true;
    bullets.physicsBodyType = Phaser.Physics.ARCADE;
    bullets.createMultiple(1000, 'bullet');
    bullets.setAll('anchor.x', 0.5);
    bullets.setAll('anchor.y', 1);
    bullets.setAll('outOfBoundsKill', true);
    bullets.setAll('checkWorldBounds', true);

    bullets1 = game.add.group();
    bullets1.enableBody = true;
    bullets1.physicsBodyType = Phaser.Physics.ARCADE;
    bullets1.createMultiple(1000, 'newbullet');
    bullets1.setAll('anchor.x', 0.5);
    bullets1.setAll('anchor.y', 1);
    bullets1.setAll('outOfBoundsKill', true);
    bullets1.setAll('checkWorldBounds', true);

    // skill
    skill_image = game.add.sprite(300,400,'skill');
    //skill_image = game.add.sprite('skill');
    skill_image.scale.setTo(10,10);
    skill_image.checkWorldBounds = true;
    game.physics.enable(skill_image, Phaser.Physics.ARCADE);
    skill_image.body.collideWorldBounds=true;
    skill_image.animations.add("boomboom",[0,1,2,3,4],4,false);
    skill_image.kill();

    //Boss
    boss=game.add.sprite(200, 200, 'bosss');
    boss.anchor.setTo(0.5, 0.5);
    boss.scale.setTo(0.3,0.3);
    boss.checkWorldBounds = true;
    game.physics.enable(boss, Phaser.Physics.ARCADE);
    boss.body.collideWorldBounds=true;
    boss.live=300;
    boss.animations.add("a",[0,1,2,3,4,5,6,7,8],8,true);
    boss.kill();
    boss.play('a');


    //  Sound
    
    shoot = game.add.audio('shoot');
    shoot.volume=0.2;

    background=game.add.audio('background');
    background.play();
    background.loopFull(0.6);


    // UI mute
    //mute=game.add.sprite(game.world.width-250, 40, 'mute');


    mute=game.add.sprite(60 ,140, 'mute');

    mute.anchor.setTo(0.5, 0.5);
    mute.inputEnabled=true;
    mute.events.onInputUp.add(function () {
        // When the paus button is pressed, we pause the game
        if(background.volume==0){background.volume=1;
            shoot.volume=0.2
        }
        else{
        background.volume=0;
        shoot.volume=0;
        }
    })

    // Pause
    pause = game.add.text(60,240,'=', { font: '84px Arial', fill: '#fff' });
    pause.anchor.setTo(0.5, 0.5);
    pause.visible = true;
    pause.inputEnabled = true;
    pause.events.onInputUp.add(function () {
        // When the paus button is pressed, we pause the game
        game.paused = pause_bool;
        pause_bool=!pause_bool;
    })
   
    



    //helper and item
    newbullet=game.add.sprite(400, 200, 'newbullet');
    newbullet.anchor.setTo(0.5, 0.5);
    newbullet.checkWorldBounds = true;
    game.physics.enable(newbullet, Phaser.Physics.ARCADE);
    newbullet.body.collideWorldBounds=true;
    newbullet.kill();
    
    helper=game.add.sprite(400, 500, 'littleship');
    helper.anchor.setTo(0.5, 0.5);
    //helper.scale.setTo(0.3,0.3);
    helper.checkWorldBounds = true;
    game.physics.enable(helper, Phaser.Physics.ARCADE);
    helper.body.collideWorldBounds=true;
    helper.kill();

    tracker=game.add.sprite(400, 500, 'tracker');
    
    tracker.checkWorldBounds = true;
    game.physics.enable(tracker, Phaser.Physics.ARCADE);
    tracker.body.collideWorldBounds=true; 
    tracker.kill();


    // The enemy's bullets
    enemyBullets = game.add.group();
    enemyBullets.enableBody = true;
    enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
    enemyBullets.createMultiple(2000, 'enemyBullet');
    enemyBullets.setAll('anchor.x', 0.5);
    enemyBullets.setAll('anchor.y', 1);
    enemyBullets.setAll('outOfBoundsKill', true);
    enemyBullets.setAll('checkWorldBounds', true);

    //  The hero!
    //game.physics.world.bounds.setTo(0, 0, 800,600);
    //game.physics.world.setBoundsCollision();
    player = game.add.sprite(400, 500, 'ship');
    player.anchor.setTo(0.5, 0.5);
    player.scale.setTo(0.3,0.3);
    player.checkWorldBounds = true;
    game.physics.enable(player, Phaser.Physics.ARCADE);
    player.body.collideWorldBounds=true;
    player.animations.add("stop",[1,6],8,false);
    player.animations.add("fire",[2,3,4,5],8,false);

    //player.play('ship', 40, false,true);
    //explosion.play('kaboom', 30, false, true);
    //  The baddies!
    aliens = game.add.group();
    aliens.enableBody = true;
    aliens.physicsBodyType = Phaser.Physics.ARCADE;

    // enemy
    enemies = game.add.group();
    enemies.enableBody = true;
    enemies.physicsBodyType = Phaser.Physics.ARCADE;
    enemies.createMultiple(30, 'invader');
    game.time.events.loop(500, this.addEnemy, this);
    var emy_tween = game.add.tween(enemies).to( { x: 100 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);


    // partical 

    
    emitter = game.add.emitter(0, 0, 300);// Create the emitter with 15 particles.
    
    emitter.makeParticles('pixel');// Set the 'pixel' image for the particles
    emitter.setYSpeed(-150, 150);
    emitter.setXSpeed(-150, 150);
    emitter.setScale(2, 0, 2, 0, 800);
    emitter.gravity = 0;

    //  The score
    scoreString = 'Score : ';
    scoreText = game.add.text(10, 10, scoreString + score, { font: '34px Arial', fill: '#fff' });

    //  Lives
    lives = game.add.group();
    game.add.text(game.world.width - 150, 10, 'Lives : ', { font: '34px Arial', fill: '#fff' });

    //  Text
    stateText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '84px Arial', fill: '#fff' });
    stateText.anchor.setTo(0.5, 0.5);
    stateText.visible = false;

    


    for (var i = 0; i < 5; i++) 
    {
        var live = lives.create(game.world.width - 150 + (30 * i), 60, 'HP');
        live.anchor.setTo(0.5, 0.5);
        
        //live.angle = 90;
        live.scale.setTo(0.1,0.1);
        live.alpha = 0.4;
    }

    //  An explosion pool
    explosions = game.add.group();
    explosions.createMultiple(300, 'kaboom');
    explosions.forEach(setupInvader, this);

    //  And some controls to play the game with
    cursors = game.input.keyboard.createCursorKeys();
    fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    skill = game.input.keyboard.addKey(Phaser.Keyboard.A);




    },
    update: function() {
        //  Scroll the background
    starfield.tilePosition.y += 1;
    if (!player.inWorld) { player.x=300;}

    if (player.alive)
    {
        //  Reset the player, then check for movement keys
        player.body.velocity.setTo(0, 0);

        if (cursors.left.isDown)
        {
            player.body.velocity.x = -200;
        }
        else if (cursors.right.isDown)
        {
            player.body.velocity.x = 200;
        }
        else if (cursors.up.isDown)
        {
            player.body.velocity.y = -200;
        }
        else if (cursors.down.isDown)
        {
            player.body.velocity.y = 200;
        }

        //  Firing?
        if (fireButton.isDown)
        {
            fireBullet();
            player.animations.play("fire");        }
        if (skill.isDown)
        {
            this.skill();
        }

        if (game.time.now > firingTimer)
        {
            this.enemyFires();
        }
        // energy bar
        //this.bar.context.clearRect(0, 0, this.bar.width, this.bar.height);
        
        // some simple colour changing to make it look like a health bar
        if (barProgress < 64) {
           this.bar.context.fillStyle = '#f00';   
        }
        else if (barProgress < 128) {
            this.bar.context.fillStyle = '#ff0';
        }
        else {
            this.bar.context.fillStyle = '#0f0';
        }
        if(enemy_count==40){
            tracker.reset(400,50);
            tracker.body.velocity.y=100;
            enemy_count++;
        }
        if(enemy_count==80){
            helper.reset(100,50);
            helper.body.velocity.y=100;
            enemy_count++;
        }
        if(enemy_count==10){
            newbullet.reset(100,50);
            
        }

        if(enemy_count==120){
            boss.reset(100,50);
            boss.play("a");
           boss_exist=true;
            enemy_count++;
        }
        
        if(boss_exist){
            if(game.time.now>boss_time){
            this.boss_move();
        }
            if(game.time.now>boss_firetime){
            this.boss_fire();

            }
        }
        
       this.bar.dirty = true;// important - without this line, the context will never be updated on the GPU when using webGL


        //
        //  Run collision
        
        game.physics.arcade.overlap(newbullet, player, this.newbullet, null, this);
        game.physics.arcade.overlap(helper, player, this.helper_help, null, this);
        game.physics.arcade.overlap(tracker, player, this.tracker_go, null, this);
        game.physics.arcade.overlap(enemyBullets, player, enemyHitsPlayer, null, this);
        game.physics.arcade.overlap(bullets,enemies, this.collisionHandler, null, {this:this,var1:this.bar,var2:this.barProgress,var3:enemies,var4:this.enermyDie});
        game.physics.arcade.overlap(bullets,boss, this.boss_collisionHandler, null, {this:this,var1:this.bar,var2:this.barProgress,var3:enemies,var4:this.enermyDie});
        game.physics.arcade.overlap(bullets1,enemies, this.collisionHandler, null, {this:this,var1:this.bar,var2:this.barProgress,var3:enemies,var4:this.enermyDie});
        game.physics.arcade.overlap(bullets1,boss, this.boss_collisionHandler, null, {this:this,var1:this.bar,var2:this.barProgress,var3:enemies,var4:this.enermyDie});
        game.physics.arcade.overlap(skill_image,enemies, this.skillcollisionHandler, null, {this:this,var1:this.bar,var2:this.barProgress,var3:enemies,var4:this.enermyDie});
        game.physics.arcade.overlap(skill_image,boss, this.skillcollisionHandler, null, {this:this,var1:this.bar,var2:this.barProgress,var3:enemies,var4:this.enermyDie});


    }

    },
    newbullet:function(){
        new_bullet=true;
        newbullet.kill();
    },
    boss_collisionHandler:function(bullet,bullett){
        console.log(boss.live)
        bullett.kill();
        //if(game.time.now>boss_time){
        
            boss.live--;
            //boss_time=game.time.now+100;
            barProgress+=3;             //  Increase the score
            this.var1.context.clearRect(0, 0, this.var1.width, this.var1.height);
            this.var1.context.fillRect(0, 0, barProgress, 8);
            //  And create an explosion :)
            var explosion = explosions.getFirstExists(false);

            explosion.reset(boss.body.x+200, boss.body.y+250);
            explosion.play('kaboom', 30, false, true);
            //console.log(boss.live,"NO");
            
            if(boss.live<=0){
                score += 20000;
                scoreText.text = scoreString + score;
                boss_exist=false;
                boss.kill();

              
        
                enemyBullets.callAll('kill',this);
                stateText.text = " You Won, \n Click to restart";
                stateText.visible = true;
        
                //the "click to restart" handler
                new_bullet=false;
                pause_bool=false;
                boss_exist=false;
                enable_help=false;
                track=false;
                background.stop();
                restart();
               

            }
        //}
    },
    boss_fire: function(){
                enemyBullet = enemyBullets.getFirstExists(false);
                enemyBullet.reset(boss.body.x+200, boss.body.y+200);
        
                game.physics.arcade.moveToObject(enemyBullet,player,120);
                //console.log(game.time.now);
              
            }
               
    ,
    boss_move: function(){
        
        var position_x=game.rnd.integerInRange(0,800);
        var v_x=game.rnd.integerInRange(-200,200);
        var v_y=game.rnd.integerInRange(-30,30);
        boss.body.velocity.x=v_x*1.2;
        boss.body.velocity.y=v_y-10;
        boss_time=game.time.now+1000;
        console.log("i am boss");
        
    },
    tracker_go: function(){
        track=true;
        tracker.kill();
        //console.log("OK");
    }
    ,
    helper_help : function(){
        helper.x=player.x+30;
        helper.y=player.y+10;
        helper.body.velocity.y=0;
        //enable_help=true;
        help_fire();

    },
    addEnemy: function() {
        // Get the first dead enemy of the group
        var enemy = enemies.getFirstDead();
        var position_x=game.rnd.integerInRange(0,800);
        var v_x=game.rnd.integerInRange(-200,200);
        var v_y=game.rnd.integerInRange(1,5);
        if (!enemy) { return;}
        if (enemy_count>=120) { return;}
        //enemy_count++;
        console.log(enemy_count);

        enemy.animations.add('fly', [ 0, 1, 2, 3], 5, true);
        enemy.play('fly');
        enemy.scale.setTo(0.5,0.5)
        enemy.anchor.setTo(0.5, 1);
        enemy.body.collideWorldBounds
        enemy.reset(position_x, 50);
        enemy.live=2;
        //enemy.body.gravity.y = 500;
        //enemy.body.velocity.x = 100 * game.rnd.pick([-1, 1]);
        enemy.checkWorldBounds = true;
        enemy.outOfBoundsKill = true;
        this.enemy_tw=game.add.tween(enemy).to( { x: game.width/2+v_x,y:300 }, 2000).to( { x: 400,y:300 }, 2000, Phaser.Easing.Linear.None).to( { x: game.width/2,y:50 }, 2000).to({x:position_x,y:50*v_y}).start();
      //  this.enemy_tw.onLoop.add(this.onLoop, this);

        },

    
    enemyFires: function() {
    
        
            livingEnemies.length=0;
        
            enemies.forEachAlive(function(enemy){
        
                // put every living enemy in an array
                livingEnemies.push(enemy);
                //livingEnemies.push(this.enemies);
    
            });
            //  Grab the first bullet we can from the pool
            //enemyBullet = enemyBullets.getFirstExists(false);
            for (i=1;i<livingEnemies.length;i++){
                enemyBullet = enemyBullets.getFirstExists(false);
                if (enemyBullet && livingEnemies.length > 0)
            {              
                var random=game.rnd.integerInRange(0,livingEnemies.length-1);
        
                // randomly select one of them
                var shooter=livingEnemies[random];
                // And fire the bullet from this enemy
                enemyBullet.reset(shooter.body.x, shooter.body.y);
        
                game.physics.arcade.moveToObject(enemyBullet,player,120);
                //console.log(game.time.now);
                firingTimer = game.time.now + firedelay;
                
                if(firedelay>300)firedelay-=20;
            }
            }     
        }
        ,
        collisionHandler:function (bullet, alien) {
    
            //  When a bullet hits an alien we kill them both
            //console.log(this,this.var1);
            this.var1.context.clearRect(0, 0, this.var1.width, this.var1.height);
            enemy_count++;
            this.var1.context.fillRect(0, 0, barProgress, 8);
            this.var4(alien);
            bullet.kill();
            if(new_bullet){
            alien.live-=2;}
            else{
                alien.live--;
            }
            if(alien.live<=0){
            alien.kill();}
            barProgress+=5;    
            score += 20;
            scoreText.text = scoreString + score;

            //var explosion = explosions.getFirstExists(false);
            //explosion.reset(alien.body.x, alien.body.y);
            //explosion.play('kaboom', 30, false, true);
            
        
        },
        skillcollisionHandler:function (bullet, alien) {
    
            this.var1.context.clearRect(0, 0, this.var1.width, this.var1.height);
            this.var1.context.fillRect(0, 0, barProgress, 8);
            if(alien!=boss)
            {
                alien.kill();
                enemy_count++;
                score += 20;
                scoreText.text = scoreString + score;

            }
            else{
                console.log(boss.live)
                boss.live-=1;

            }
            
            
            //console.log(t)
            //  And create an explosion :)
            var explosion = explosions.getFirstExists(false);
            //explosion.reset(alien.body.x, alien.body.y);
            //explosion.play('kaboom', 30, false, true);
        
            //if (aliens.countLiving() == 0)
            
        },
       
        skill:function(){
           
            
            
            skill_image.play('boomboom');
            if(barProgress>=128){
               // console.log("Bang");
                barProgress=5;
                skill_image.reset(game.width/2-100, game.height/2-100);
                setTimeout(function(){skill_image.kill()}, 1200 )
                

            }
        },
        enermyDie:function(enermy) {
            
            emitter.x = enermy.x;
            emitter.y = enermy.y;
            emitter.start(true, 800, null, 15);
            
            },
        
            
    
    };
    //var game = new Phaser.Game(800, 600, Phaser.AUTO, 'phaser-example');
    //game.state.add('main', mainState);
    //game.state.start('main');
    

    function createAliens () {

        for (var y = 0; y < 4; y++)
        {
            for (var x = 0; x < 10; x++)
            {
                var alien = aliens.create(x * 48, y * 50, 'invader');
                alien.anchor.setTo(0.5, 0.5);
                alien.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
                alien.play('fly');
                alien.body.moves = false;
            }
        }
    
        aliens.x = 50;
        aliens.y = 50;
    
        //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
        var tween = game.add.tween(aliens).to( { x: 100 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);
    
        //  When the tween loops it calls descend
        //tween.onLoop.add(descend, this);
    }
    
    function setupInvader (invader) {
    
        invader.anchor.x = 0.5;
        invader.anchor.y = 0.5;
        invader.animations.add('kaboom');
    
    }
    
    function descend() {
    
        aliens.y += 10;
    
    }
    
    function render() {

        // for (var i = 0; i < aliens.length; i++)
        // {
        //     game.debug.body(aliens.children[i]);
        // }
    
    }
     
    function enemyHitsPlayer (player,bullet) {
        
        bullet.kill();
    
        live = lives.getFirstAlive();
    
        if (live)
        {
            live.kill();
        }
    
        //  And create an explosion :)
        var explosion = explosions.getFirstExists(false);
        explosion.reset(player.body.x, player.body.y);
        explosion.play('kaboom', 30, false, true);
    
        // When the player dies
        if (lives.countLiving() < 1)
        {
            player.kill();
            background.stop();

            enemyBullets.callAll('kill');
            enemy_count=0;
            barProgress=5;
            //score=0;

            stateText.text=" GAME OVER \n Click to restart";
            stateText.visible = true;
            //enemy_count=0;
            //game.state.start('menu');
            //the "click to restart" handler
            new_bullet=false;
            pause_bool=false;
            boss_exist=false;
            enable_help=false;
            track=false;
            game.input.onTap.addOnce(function(){game.state.start('menu');});
            
        }
    
    }
    
    function enemyFires () {
    
        //  Grab the first bullet we can from the pool
        enemyBullet = enemyBullets.getFirstExists(false);
    
        livingEnemies.length=0;
    
        aliens.forEachAlive(function(alien){
    
            // put every living enemy in an array
            livingEnemies.push(alien);
            livingEnemies.push(enemies);

        });
    
    
        if (enemyBullet && livingEnemies.length > 0)
        {
            
            var random=game.rnd.integerInRange(0,livingEnemies.length-1);
    
            // randomly select one of them
            var shooter=livingEnemies[random];
            // And fire the bullet from this enemy
            enemyBullet.reset(shooter.body.x, shooter.body.y);
    
            game.physics.arcade.moveToObject(enemyBullet,player,120);
            //console.log(game.time.now);
            firingTimer = game.time.now + firedelay;
            if(firedelay>200){firedelay-=200;}
            console.log(firedelay)
        }
    
    }
    
    function fireBullet () {
    
        //  To avoid them being allowed to fire too fast we set a time limit
        if (game.time.now > bulletTime)
        {
            //  Grab the first bullet we can from the pool
            if(new_bullet){
                bullet = bullets1.getFirstExists(false);
            }
            else{
            bullet = bullets.getFirstExists(false);
        }
    
            if (bullet)
            {
                //  And fire it
                shoot.play();
                if(track){
                target=get_enermy(player.x,player.y,600);
                if(target){
                    bullet.reset(player.x, player.y + 8);
                   // bullet.body.velocity.y = -1200;
                    bulletTime = game.time.now + 200;
                    bullet.body.velocity.y = (target.y-player.y)*2.3;
                    bullet.body.velocity.x = (target.x-player.x)*2.3;
        
                }
                }
                else{
                bullet.reset(player.x, player.y + 8);
                bullet.body.velocity.y = -1200;
                bulletTime = game.time.now + 200;
            }
            }
        }
    
    }

    function help_fire () {
    
        //  To avoid them being allowed to fire too fast we set a time limit
        if (game.time.now > help_bulletTime)
            {
            //  Grab the first bullet we can from the pool
            bullet = bullets.getFirstExists(false);
    
            if (bullet)
            {
                //  And fire it
                shoot.play();

                bullet.reset(helper.x, helper.y + 8);
                bullet.body.velocity.y = -1200;
                help_bulletTime = game.time.now + 200;
            }
        }
        
    
    }

    
    function resetBullet (bullet) {
    
        //  Called if the bullet goes out of the screen
        bullet.kill();
    
    }
    
    function restart () {
    
        //  A new level starts
       // score=0;
        //resets the life count
        lives.callAll('revive');
        //  And brings the aliens back from the dead :)
        enemies.removeAll();
        ;
        firedelay=2000;
        enemy_count=0;
        //revives the player
        player.revive();
        //hides the text
        stateText.visible = false;
        game.input.onTap.addOnce(function(){game.state.start('menu');});    
    }

    function get_enermy(x,y,distance){
        var enemyUnits=[];
        
        enemies.forEach(/// game.enemies 是自已加的
            child=>{
                enemyUnits.push(child);
            }
        );
        enemyUnits.push(boss);
            //var enemyUnits = this.game.enemies.getChildren();
            for(var i = 0; i < enemyUnits.length; i++) {
                //console.log(enemyUnits[i].x);       
                if( Phaser.Math.distance(x, y, enemyUnits[i].x, enemyUnits[i].y) <= distance)
                {
                    //console.log("here",Phaser.Math.distance(x, y, enemyUnits[i].x, enemyUnits[i].y));       
        
                return enemyUnits[i];}
                    
            }
            return false;
        
    }